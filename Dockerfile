FROM gitlab/gitlab-runner:alpine
RUN mkdir /deploy
RUN chown -R -v gitlab-runner /deploy
